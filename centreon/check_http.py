#!/usr/bin/env python2.7
"""
Probe used to check valid error code when accessing http endpoint. Works as well
with https. 
Needs functions.py additional module.
Python 2 compatible
"""
import urllib2
import argparse
from functions import *

parser = argparse.ArgumentParser()
parser.add_argument('-H', '--host', required=True, type=str, help='Target hostname')
parser.add_argument('-P', '--port', default=80, type=int, help='Target port. Default to port 80')
args = parser.parse_args()

prepare()

if args.port == 443:
    proto = 'https'
else:
    proto = 'http'

url = "{}://{}:{}".format(proto, args.host, args.port)

try:
    ret = urllib2.urlopen(url)
except urllib2.URLError as error:
    error = str(error)
    if 'Connection refused' in error:
        tear_down(2)
    else:
        tear_down(3)

if ret.getcode() == 200:
    tear_down(0, 'All systems go')
else:
    tear_down(2, 'Unknown output, please investigate')