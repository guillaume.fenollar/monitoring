#!/usr/local/bin/python2.7
# -*- coding:utf8 -*-
"""
Probe used to count number of lines in an Oracle database in order to detect when too many records are there
"""
try:
  import cx_Oracle
except:
  print('Import of lib cx_Oracle failed')
  exit(1)

import argparse
from functions import *

parser = argparse.ArgumentParser()
parser.add_argument('-H', '--host', type=str, help='Oracle Machine')
parser.add_argument('-P', '--port', default=1521, type=int, help='Target port. Default to port 1521')
parser.add_argument('-d', '--db', help="Database to connecto to")
parser.add_argument('-u', '--user', required=True, help='User to use for connection to Oracle')
parser.add_argument('-p', '--passwd', required=True, help='Password')
parser.add_argument('-t', '--table', required=True, help='Table to count')
parser.add_argument('-w', '--warning', type=int, required=True, help='Warning threshold')
parser.add_argument('-c', '--critical', type=int, required=True, help='Critical threshold')
args = parser.parse_args()

connection = cx_Oracle.connect(args.user, args.passwd, "{}:{}/{}".format(args.host, args.port, args.db))

cursor = connection.cursor()
cursor.execute("select count(*) from {}.{}".format(args.db.lower(), args.table))
result = cursor.fetchall()[0][0]
cursor.close()
connection.close()

mess = "Lines in table {}.{}".format(args.db.lower(), args.table)
status = evaluate(args.warning, args.critical, result)
perf = '\'{}\'={};{};{}'.format(mess, result, args.warning, args.critical)
tear_down(status, "{} {}".format(result, mess), perf)