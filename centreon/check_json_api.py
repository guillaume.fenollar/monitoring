#!/usr/bin/env python2.7
"""
Check that the API returns correct Json, thus validating HTTP + basic response.
Better version than check_http because it goes through application layer.
Python 2 compatible
"""

import urllib2
import argparse
from json import loads
from functions import *

parser = argparse.ArgumentParser()
parser.add_argument('-H', '--host', required=True, type=str, help='Target hostname')
parser.add_argument('-P', '--port', default=80, type=int, help='Target port. Default to port 80')
parser.add_argument('-U', '--url', default='/', help="API Endpoint for the check")
args = parser.parse_args()

prepare()

if args.port == 443:
    proto='https'
else:
    proto='http'

url = "{}://{}:{}{}".format(proto, args.host, args.port, args.url)

ret = url_open(url)


if ret.getcode() == 200:
    try:
        cjson = loads(ret.read())
    except:
        tear_down(2, 'API returned an invalid json')

    tear_down(0, '')

else:
    tear_down(2, "Error code {} when accessing Couchbase API".format(code))
