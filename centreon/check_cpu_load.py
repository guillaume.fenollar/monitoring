#!/usr/bin/env python2

import argparse
import os
from math import ceil

parser = argparse.ArgumentParser()
parser.add_argument('-H', '--host', help='Target hostname')
parser.add_argument('-w', '--warning', default=125, type=int, help='Abnormal cpu usage in percent (int)')
parser.add_argument('-c', '--critical', default=200, type=int, help='Critical cpu usage in percent (int)')
args = parser.parse_args()

with open('/proc/loadavg') as f:
    loadavg = f.read().split()
    
load_5m = float(loadavg[0])
load_10m = float(loadavg[1])
load_15m = float(loadavg[2])
runnable = loadavg[3].split('/')[0]
cpus = float(os.sysconf("SC_NPROCESSORS_ONLN"))

usage_5m = 100 * load_5m / cpus
usage_10m = 100 * load_10m / cpus
usage_15m = 100 * load_15m / cpus

def print_usage(status, exitcode):
    warn = 100
    crit = 200
    perfdata5 = "cpu5m={0}%;{1};{2}".format(ceil(usage_5m), warn, crit)
    perfdata10 = "cpu10m={0}%;{1};{2}".format(ceil(usage_10m), warn, crit)
    perfdata15 = "cpu15m={0}%;{1};{2}".format(ceil(usage_15m), warn, crit)
    print('{0} - CPU Usage {1}% - {5}R|{2} {3} {4}'.format(status.upper(), usage_15m, perfdata5, perfdata10, perfdata15, runnable))
    exit(exitcode)

if usage_15m > args.critical:
    print_usage('critical',2)
elif usage_15m > args.warning:
    print_usage('warning', 1)
else:
    print_usage('ok', 0)