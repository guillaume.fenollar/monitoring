"""
    Reusable Python Functions
"""

JOLOKIA_ENDPOINT='http://jolokia:8989/jolokia'


def prepare():
    from socket import setdefaulttimeout
    from os import environ
    setdefaulttimeout(10) # Timeout pour les connexions, en secondes
    if 'http_proxy' in environ: del environ['http_proxy'] # Suppression de http_proxy de l'env


def get_metric(mbean, **kwargs):
    from json import loads
    data = {
        "type": "read",
        "target": {"url": "service:jmx:rmi:///jndi/rmi://{}:{}/jmxrmi".format(kwargs['host'], kwargs['port'])}
    }
    split_mbean = mbean.split(',')
    data['mbean'] = split_mbean[0]
    for i in split_mbean:
        if i.startswith('name='):
            data['mbean'] = '{},{}'.format(data['mbean'], i)
        elif i.startswith('attribute='):
            data['attribute'] = i.split('=')[1]
        elif i.startswith('path='):
            data['path'] = i.split('=')[1]

    if 'attribute' in kwargs:
        data['attribute'] = kwargs['attribute']

    if 'path' in kwargs:
        data['path'] = kwargs['path']

    ret = url_open(JOLOKIA_ENDPOINT, data)
    output = loads(ret.read())
    if output['status'] != 200:
        tear_down(2, output['error'])

    return output['value']


def url_open(url, data={}):
    import urllib2
    import json
    try:
        if data:
            req = urllib2.Request(url, json.dumps(data))
            req.add_header('Content-Type', 'application/json')
        else:
            req = urllib2.Request(url)
        ret = urllib2.urlopen(req)
    except urllib2.URLError as error:
        error = str(error)
        if 'Connection refused' in error:
            tear_down(2, 'Cannot reach the HTTP target')
        elif 'timed out' in error:
            tear_down(2, 'Timeout during HTTP connection to the target')
        else:
            tear_down(3, error)
    else:
        return ret


def evaluate(w,c,result, low=None):
    code = 3
    if w <= c:
        if result > c:
            code = 2
        elif result > w:
            code = 1
        elif low:
            if result < low:
                code = 2
            else:
                code = 0
        else:
            code = 0
    else:
        if result < c:
            code = 2
        elif result < w:
            code = 1
        elif low:
            if result < low:
                code = 2
            else:
                code = 0
        else:
            code = 0
    return code


def tear_down(status, mess='', perf=''):
   mess = str(mess)

   if perf:
      perf = '| ' + perf

   if status == 0:
       print("OK - {} {}".format(mess, perf))
       exit(0)
   elif status == 1:
       print("WARNING - {} {}".format(mess, perf))
       exit(1)
   elif status == 2:
       print("CRITICAL - {} {}".format(mess, perf))
       exit(2)
   else:
       print("UNKNOWN - {} {}".format(mess))
       exit(3)