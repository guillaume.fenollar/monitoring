#!/usr/bin/env python

""" Written by Guillaume Fenollar <guillaume@fenollar.fr>
Check any tcp port openness, with a default timeout of 5s
Python 2/3 compatible
"""
import socket
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('-H', '--host', required=True, help='Target hostname')
parser.add_argument('-P', '--port', default=80, type=int, help='Target port. Default to port 80')
parser.add_argument('-t', '--timeout', default=5, type=int, help='Socket timeout after which the port is considered closed. Default to 5 (s)')
args = parser.parse_args()

def tear_down(e):
    if(status):
        sock.close()
    exit(e)

socket.setdefaulttimeout(args.timeout)
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

try:
    status = sock.connect_ex((args.host, args.port))
except socket.gaierror as error:
    print("CRITICAL - {} can't be joined".format(args.host, error))
    tear_down(3)

if status == 0:
    print("OK - port {} is open".format(args.port))
    tear_down(0)
else:
    print("CRITICAL - port {} is closed".format(args.port))
    tear_down(2)