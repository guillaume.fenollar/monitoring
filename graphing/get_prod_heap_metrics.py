#!/usr/bin/env python
# encoding: utf-8

""" Programme multi-threadé qui interroge l'API Jolokia pour fetch les stats de mémoire vive de JVM afin de les envoyer dans graphite via statsd.
 Le pillar de Salt est utilisé pour récupérer la liste des applications en temps réel. Seuls les ports ouverts sont interrogés."""

import socket
import threading
import json
import requests
import statsd
import time
import sdnotify
from salt import client

# Timeout de connexion à Jolokia pour récupérer les informations d'une JVM
JK_TIMEOUT = 4
# Adresse de Jolokia, le reverse proxy JMX
JOLOKIA_ENDPOINT='http://changeme:8989/jolokia'
# Timeout de check de port, avant connexion à Jolokia
SOCKET_TIMEOUT = 2
# Interval entre les checks 
INTERVAL=55

ENV = 'prod'
apl1 = 'changeme'
apl2 = 'changeme'


socket.setdefaulttimeout(JK_TIMEOUT)

def get_salt_applications_info(app):
    # Extrait depuis Salt la config des applications NEED
    local = client.Caller()
    return local.cmd('pillar.get', app)


def extract_config():
    # Prend la liste des applications en entrée et génère un dict avec les app/ports à requêter
    salt_data = get_salt_applications_info('applications')
    apps = {}
    for current_app in salt_data:
        for macro in current_app:
            apps[macro] = {}
            for micro in current_app[macro].iteritems():
                if micro[0] == '_machine':
                    micro_machine = micro[1]

            for micro in current_app[macro].iteritems():
                if micro[0] != '_machine':
                    for config in micro[1].iteritems():
                        if config[0] == 'jmx':
                            apps[macro][micro[0]] = '{}:{}'.format(micro_machine, config[1]['port'])
        if len(apps[macro]) < 2:
            # Si on a pas d'autre microapplication que le nom meta 'machine', alors on supprime l'app de la liste
            del apps[macro]
    
    return apps
 

class ThreadedHeapCheck(threading.Thread):
    # Classe permettant de lancer les tests/requêtes jolokia en parallèle
    def __init__(self, machine, port, macro, micro):
        threading.Thread.__init__(self)
        self.machine = machine
        self.port = port
        self.macro = macro
        self.micro = micro[0]

    def run(self):
        if self.port_is_open():
            values = self.get_metric()
            self.send_to_statsd(values)


    def port_is_open(self):
        socket.setdefaulttimeout(SOCKET_TIMEOUT)
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        result = sock.connect_ex((self.machine, self.port))
        if result == 0:
            return True
        else:
            return False

    def get_metric(self):
        data = {
            "type": "read",
            "target": {"url": "service:jmx:rmi:///jndi/rmi://{}:{}/jmxrmi".format(self.machine, self.port)},
            "mbean": "java.lang:type=Memory"
        }
    
        r = requests.post(JOLOKIA_ENDPOINT, data=json.dumps(data), headers={'Content-Type':'application/json'})
        j = json.loads(r.text)
        if j['status'] == 200:
            return j['value']['HeapMemoryUsage']

    def send_to_statsd(self, values):
        for v in values.iteritems():
            stats.gauge('heap.{}.{}.{}.{}'.format(ENV, self.macro, self.micro, v[0]), v[1])


if __name__ == '__main__':
    stats = statsd.StatsClient(host='localhost',
                   port=8125,
                   prefix='need')
    n = sdnotify.SystemdNotifier()
    n.notify('READY=1')
    while True:
        time_start = time.time()
        threads = []
        all_apps = extract_config()
        for macro in all_apps:
            for micro in all_apps[macro].iteritems():
                try:
                    machine = micro[1].split(':')[0]
                    if machine == 'apl1':
                        machine = apl1
                    elif machine == 'apl2':
                        machine = apl2
                    port = int(micro[1].split(':')[1])
                except:
                    # Si problème lors de la récup des infos, on skip
                    print('Les data suivantes semblent non valides :\n{}'.format(micro))
                    continue
                thread = ThreadedHeapCheck(machine, port, macro, micro)
                thread.start()
                threads.append(thread)
        for t in threads:
            t.join()
        n.notify('WATCHDOG=1')
        # On calcule le temps passé par le script a travailler pour le soustraire à l'intervalle défini en constante
        time_end = time.time()
        time_elapsed = time_end - time_start
        print('Iteration terminée en {}s, {} applications scannées'.format(time_elapsed, len(threads)))
        time.sleep(INTERVAL - time_elapsed)

